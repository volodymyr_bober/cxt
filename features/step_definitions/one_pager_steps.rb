Given(/^remote control is opened$/) do                                                                                                                                                                                                         
  visit 'http://176.34.94.213:8888/staticqa/index.html'
  current_path.should == '/staticqa/index.html'
  puts "current path: #{current_path}"
end                                                                                                                                                                                                                                            
                                                                                                                                                                                                                                               
When(/^I switch the trigger$/) do                                                                                                                                                                                                              
  altitude_gauge_trigger = page.find 'label[for=toggle-altitude-gauge-update]'
  @alt_gauge_trigger_state_before = altitude_gauge_trigger.text
  page.save_screenshot('./screen.png')
  altitude_gauge_trigger.click
end                                                                                                                                                                                                                                            
                                                                                                                                                                                                                                               
Then(/^the trigger gets switched$/) do                                                                                                                                                                                                         
  altitude_gauge_trigger = page.find 'label[for=toggle-altitude-gauge-update]'
  altitude_gauge_trigger.text.should_not == @alt_gauge_trigger_state_before
end 